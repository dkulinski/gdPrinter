include <T-Slot.scad>
include <MCAD/stepper.scad>

showFullModel=0;

//mirror([0,1,0])translate([0,5,0])motorBracket();
motorBracket();
if(showFullModel == 1)
{
    //sides
    translate([200,180,210])2020Profile(height=380);
    translate([200,-200,210])2020Profile(height=380);
    translate([-180,-200,210])2020Profile(height=380);
    translate([-180,180,210])2020Profile(height=380);

    // Base and top
    translate([-180,0,10])rotate([90,0,0])2020Profile(height=380);
    translate([20,180,10])rotate([90,0,90])2020Profile(height=380);
    translate([0,-200,10])rotate([90,0,90])2020Profile(height=380);
    translate([200,-20,10])rotate([90,0,0])2020Profile(height=380);

    translate([-180,-20,410])rotate([90,0,0])2020Profile(height=380);
    translate([0,180,410])rotate([90,0,90])2020Profile(height=380);
    translate([20,-200,410])rotate([90,0,90])2020Profile(height=380);
    translate([200,0,410])rotate([90,0,0])2020Profile(height=380);

    // Z Motors
    translate([238,0,55])rotate([0,180,0])motor(Model=Nema17);
    translate([-218,0,55])rotate([0,180,0])motor(Model=Nema17);

    // XY Motors
    translate([200,218,420])rotate([0,180,0])motor(Model=Nema17);
    translate([-180,218,420])rotate([0,180,0])motor(Model=Nema17);
}

module motorBracket(motorSide=42.3,motorHeight=47, wallThickness=5, motorHeadDiameter=23, screwDiameter=3.0, screwHoleSpace=31.0) {
    //difference() {
        union() {
            polyhedron( 
                points = [ 
                
                    [0,0,0],
                    [motorSide,0,0],
                    [motorSide,motorSide,0],
                    [motorSide,0,-motorHeight],
                    [0,wallThickness,0],
                    [motorSide,wallThickness,0],
                    [motorSide,motorSide+wallThickness,0],
                    [motorSide+wallThickness,motorSide+wallThickness,0],
                    [motorSide+wallThickness,0,0],
                    [motorSide+wallThickness,wallThickness,0],
                    [2*motorSide+wallThickness,0,0],
                    [2*motorSide+wallThickness,wallThickness,0],
                    [motorSide,wallThickness,-motorHeight],
                    [motorSide+wallThickness,wallThickness,-motorHeight],
                    [motorSide+wallThickness,0,-motorHeight]
                ],
                faces = [ [0,4,5], [5,1,0], [1,6,8], [6,7,8], [8,9,11], [11,10,8], [0,1,3], [1,8,14], [14,3,1],[8,10,14], [5,12,6], [9,7,13], [4,12,5], [9,13,11], [0,3,12], [0,12,4], [7,6,12], [7,12,13], [10,11,13], [10,13,14], [3,12,13], [3,13,14] ]);
                cube( [2*motorSide+wallThickness, motorSide+wallThickness, wallThickness]);
        }
        color([1,0,0])translate([motorSide/2,motorSide/2+wallThickness,-0.5])polyhole(h=wallThickness+1,d=motorHeadDiameter);
        screwFromEdge=(motorSide-screwHoleSpace)/2;
        color([0,1,0])translate([screwFromEdge,screwFromEdge+wallThickness,-0.5])polyhole(h=wallThickness+1,d=screwDiameter);
        color([0,1,0])translate([motorSide-screwFromEdge,screwFromEdge+wallThickness,-0.5])polyhole(h=wallThickness+1,d=screwDiameter);
        color([0,1,0])translate([motorSide-screwFromEdge,motorSide-screwFromEdge+wallThickness,-0.5])polyhole(h=wallThickness+1,d=screwDiameter);
        color([0,1,0])translate([screwFromEdge,motorSide-screwFromEdge+wallThickness,-0.5])polyhole(h=wallThickness+1,d=screwDiameter);
    //}
}

module polyhole(h, d) {
    n = max(round(2 * d),3);
    rotate([0,0,180])
        cylinder(h = h, r = (d / 2) / cos (180 / n), $fn = n);
}